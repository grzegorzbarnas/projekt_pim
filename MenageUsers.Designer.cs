﻿namespace ShopMenager
{
    partial class MenageUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Tname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Tphone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Tpassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Tfullname = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.shopMenagerdbDataSet2 = new ShopMenager.ShopMenagerdbDataSet2();
            this.userTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.userTableTableAdapter = new ShopMenager.ShopMenagerdbDataSet2TableAdapters.UserTableTableAdapter();
            this.shopMenagerdbDataSet3 = new ShopMenager.ShopMenagerdbDataSet3();
            this.userTableBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.userTableTableAdapter1 = new ShopMenager.ShopMenagerdbDataSet3TableAdapters.UserTableTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.userTableBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.shopMenagerdbDataSet1 = new ShopMenager.ShopMenagerdbDataSet1();
            this.userTableTableAdapter2 = new ShopMenager.ShopMenagerdbDataSet1TableAdapters.UserTableTableAdapter();
            this.shopMenagerdbDataSet = new ShopMenager.ShopMenagerdbDataSet();
            this.shopMenagerdbDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.unameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ufullnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.upasswordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uphoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopMenagerdbDataSet4 = new ShopMenager.ShopMenagerdbDataSet4();
            this.userTableBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.userTableTableAdapter3 = new ShopMenager.ShopMenagerdbDataSet4TableAdapters.UserTableTableAdapter();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(917, 80);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(880, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 24);
            this.label6.TabIndex = 2;
            this.label6.Text = "X";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Book", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(360, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Zarządzaj użytkownikami";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nazwa użytkownika:";
            // 
            // Tname
            // 
            this.Tname.Location = new System.Drawing.Point(122, 108);
            this.Tname.Name = "Tname";
            this.Tname.Size = new System.Drawing.Size(136, 20);
            this.Tname.TabIndex = 3;
            this.Tname.TextChanged += new System.EventHandler(this.Tname_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Numer telefonu:";
            // 
            // Tphone
            // 
            this.Tphone.Location = new System.Drawing.Point(122, 206);
            this.Tphone.Name = "Tphone";
            this.Tphone.Size = new System.Drawing.Size(136, 20);
            this.Tphone.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Hasło:";
            // 
            // Tpassword
            // 
            this.Tpassword.Location = new System.Drawing.Point(122, 176);
            this.Tpassword.Name = "Tpassword";
            this.Tpassword.Size = new System.Drawing.Size(136, 20);
            this.Tpassword.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Imie Nazwisko:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // Tfullname
            // 
            this.Tfullname.Location = new System.Drawing.Point(122, 143);
            this.Tfullname.Name = "Tfullname";
            this.Tfullname.Size = new System.Drawing.Size(136, 20);
            this.Tfullname.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(69, 242);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(150, 242);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Edytuj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(231, 242);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Usuń";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(150, 280);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Menu";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 417);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(917, 33);
            this.panel2.TabIndex = 15;
            // 
            // shopMenagerdbDataSet2
            // 
            this.shopMenagerdbDataSet2.DataSetName = "ShopMenagerdbDataSet2";
            this.shopMenagerdbDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // userTableBindingSource
            // 
            this.userTableBindingSource.DataMember = "UserTable";
            this.userTableBindingSource.DataSource = this.shopMenagerdbDataSet2;
            // 
            // userTableTableAdapter
            // 
            this.userTableTableAdapter.ClearBeforeFill = true;
            // 
            // shopMenagerdbDataSet3
            // 
            this.shopMenagerdbDataSet3.DataSetName = "ShopMenagerdbDataSet3";
            this.shopMenagerdbDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // userTableBindingSource1
            // 
            this.userTableBindingSource1.DataMember = "UserTable";
            this.userTableBindingSource1.DataSource = this.shopMenagerdbDataSet3;
            // 
            // userTableTableAdapter1
            // 
            this.userTableTableAdapter1.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.unameDataGridViewTextBoxColumn,
            this.ufullnameDataGridViewTextBoxColumn,
            this.upasswordDataGridViewTextBoxColumn,
            this.uphoneDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.userTableBindingSource3;
            this.dataGridView1.Location = new System.Drawing.Point(428, 115);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(442, 150);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // userTableBindingSource2
            // 
            this.userTableBindingSource2.DataMember = "UserTable";
            this.userTableBindingSource2.DataSource = this.shopMenagerdbDataSet1;
            // 
            // shopMenagerdbDataSet1
            // 
            this.shopMenagerdbDataSet1.DataSetName = "ShopMenagerdbDataSet1";
            this.shopMenagerdbDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // userTableTableAdapter2
            // 
            this.userTableTableAdapter2.ClearBeforeFill = true;
            // 
            // shopMenagerdbDataSet
            // 
            this.shopMenagerdbDataSet.DataSetName = "ShopMenagerdbDataSet";
            this.shopMenagerdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shopMenagerdbDataSetBindingSource
            // 
            this.shopMenagerdbDataSetBindingSource.DataSource = this.shopMenagerdbDataSet;
            this.shopMenagerdbDataSetBindingSource.Position = 0;
            // 
            // unameDataGridViewTextBoxColumn
            // 
            this.unameDataGridViewTextBoxColumn.DataPropertyName = "Uname";
            this.unameDataGridViewTextBoxColumn.HeaderText = "Uname";
            this.unameDataGridViewTextBoxColumn.Name = "unameDataGridViewTextBoxColumn";
            // 
            // ufullnameDataGridViewTextBoxColumn
            // 
            this.ufullnameDataGridViewTextBoxColumn.DataPropertyName = "Ufullname";
            this.ufullnameDataGridViewTextBoxColumn.HeaderText = "Ufullname";
            this.ufullnameDataGridViewTextBoxColumn.Name = "ufullnameDataGridViewTextBoxColumn";
            // 
            // upasswordDataGridViewTextBoxColumn
            // 
            this.upasswordDataGridViewTextBoxColumn.DataPropertyName = "Upassword";
            this.upasswordDataGridViewTextBoxColumn.HeaderText = "Upassword";
            this.upasswordDataGridViewTextBoxColumn.Name = "upasswordDataGridViewTextBoxColumn";
            // 
            // uphoneDataGridViewTextBoxColumn
            // 
            this.uphoneDataGridViewTextBoxColumn.DataPropertyName = "Uphone";
            this.uphoneDataGridViewTextBoxColumn.HeaderText = "Uphone";
            this.uphoneDataGridViewTextBoxColumn.Name = "uphoneDataGridViewTextBoxColumn";
            // 
            // shopMenagerdbDataSet4
            // 
            this.shopMenagerdbDataSet4.DataSetName = "ShopMenagerdbDataSet4";
            this.shopMenagerdbDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // userTableBindingSource3
            // 
            this.userTableBindingSource3.DataMember = "UserTable";
            this.userTableBindingSource3.DataSource = this.shopMenagerdbDataSet4;
            // 
            // userTableTableAdapter3
            // 
            this.userTableTableAdapter3.ClearBeforeFill = true;
            // 
            // MenageUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Tfullname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Tpassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Tphone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Tname);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MenageUsers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenageUsers";
            this.Load += new System.EventHandler(this.MenageUsers_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userTableBindingSource3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Tname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Tphone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Tpassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Tfullname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel2;
        private ShopMenagerdbDataSet2 shopMenagerdbDataSet2;
        private System.Windows.Forms.BindingSource userTableBindingSource;
        private ShopMenagerdbDataSet2TableAdapters.UserTableTableAdapter userTableTableAdapter;
        private ShopMenagerdbDataSet3 shopMenagerdbDataSet3;
        private System.Windows.Forms.BindingSource userTableBindingSource1;
        private ShopMenagerdbDataSet3TableAdapters.UserTableTableAdapter userTableTableAdapter1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private ShopMenagerdbDataSet1 shopMenagerdbDataSet1;
        private System.Windows.Forms.BindingSource userTableBindingSource2;
        private ShopMenagerdbDataSet1TableAdapters.UserTableTableAdapter userTableTableAdapter2;
        private System.Windows.Forms.BindingSource shopMenagerdbDataSetBindingSource;
        private ShopMenagerdbDataSet shopMenagerdbDataSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn unameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ufullnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn upasswordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uphoneDataGridViewTextBoxColumn;
        private ShopMenagerdbDataSet4 shopMenagerdbDataSet4;
        private System.Windows.Forms.BindingSource userTableBindingSource3;
        private ShopMenagerdbDataSet4TableAdapters.UserTableTableAdapter userTableTableAdapter3;
    }
}