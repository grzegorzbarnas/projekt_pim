﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ShopMenager
{
    public partial class MenageProducts : Form
    {
        public MenageProducts()
        {
            InitializeComponent();
        }
        SqlConnection Con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=C:\Users\barna\source\repos\ShopMenager\ShopMenagerdb.mdf;Integrated Security = True; Connect Timeout=30");
        void populate()
        {
            try
            {
                Con.Open();
                string Myquery = "select * from Product_Table";
                SqlDataAdapter da = new SqlDataAdapter(Myquery, Con);
                SqlCommandBuilder builder = new SqlCommandBuilder(da);
                var ds = new DataSet();
                da.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                Con.Close();
            }
            catch
            {

            }
        }
        private void MenageProducts_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'shopMenagerdb.Product_Table' table. You can move, or remove it, as needed.
            this.product_TableTableAdapter1.Fill(this.shopMenagerdb.Product_Table);
            // TODO: This line of code loads data into the 'shopMenagerdbDataSet.Product_Table' table. You can move, or remove it, as needed.
            // this.product_TableTableAdapter.Fill(this.shopMenagerdbDataSet.Product_Table);
            populate();// TODO: This line of code loads data into the 'shopMenagerdbDataSet.Product_Table' table. You can move, or remove it, as needed.
                       // this.product_TableTableAdapter.Fill(this.shopMenagerdbDataSet.Product_Table);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
       
         
       

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Con.Open();
                SqlCommand cmd = new SqlCommand("insert into Product_Table values(" + ProdIdTb.Text + ",'" + ProdNameTb.Text + "','" + ProdSellTb.Text + "','" + ProdBuyTb.Text + "'," + ProdStockTb.Text + ")", Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Produkt dodany");
                Con.Close();
                populate();
            }
            catch
            {

            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            m.Show();
            this.Hide();
        }

       

        private void button3_Click(object sender, EventArgs e)
        {
           if (ProdIdTb.Text == "")
            {
                MessageBox.Show("Wprowadź kod produktu");
            }
            else
            {
                Con.Open();
                string myquery = "delete from Product_Table where P_id='" + ProdIdTb.Text + "';";
                SqlCommand cmd = new SqlCommand(myquery, Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Produkt usunięty");
                Con.Close();
                populate();
           }
        }
       

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
           if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                dataGridView1.CurrentRow.Selected = true;
                ProdIdTb.Text = dataGridView1.Rows[e.RowIndex].Cells["pidDataGridViewTextBoxColumn"].FormattedValue.ToString();
                ProdNameTb.Text = dataGridView1.Rows[e.RowIndex].Cells["pnameDataGridViewTextBoxColumn"].FormattedValue.ToString();
                ProdSellTb.Text = dataGridView1.Rows[e.RowIndex].Cells["ppricesellDataGridViewTextBoxColumn"].FormattedValue.ToString();
                ProdBuyTb.Text = dataGridView1.Rows[e.RowIndex].Cells["ppricebuyDataGridViewTextBoxColumn"].FormattedValue.ToString();
                ProdStockTb.Text = dataGridView1.Rows[e.RowIndex].Cells["pstockDataGridViewTextBoxColumn"].FormattedValue.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Con.Open();
                SqlCommand cmd = new SqlCommand("update Product_Table set P_id='" + ProdIdTb.Text + "',P_name='" + ProdNameTb.Text + "',P_pricesell='" + ProdSellTb.Text + "',P_pricebuy='" + ProdBuyTb.Text + "',P_stock='" + ProdStockTb.Text + "' where P_id='"+ ProdIdTb.Text+"'", Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Produkt zmodyfikowany");
                Con.Close();
                populate();
            }
            catch
            {

            }
        }
        private void ProdIdTb_TextChanged(object sender, EventArgs e)
        {

        }

        private void ProdNameTb_TextChanged(object sender, EventArgs e)
        {

        }

        private void ProdSellTb_TextChanged(object sender, EventArgs e)
        {

        }

        private void ProdBuyTb_TextChanged(object sender, EventArgs e)
        {

        }

        private void ProdStockTb_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            m.Show();
            this.Hide();
        }
    }
}
