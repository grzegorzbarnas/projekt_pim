﻿namespace ShopMenager
{
    partial class MenageProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ProdNameTb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ProdSellTb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ProdIdTb = new System.Windows.Forms.TextBox();
            this.shopMenagerdbDataSet = new ShopMenager.ShopMenagerdbDataSet();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ProdBuyTb = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.ProdStockTb = new System.Windows.Forms.TextBox();
            this.productTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.product_TableTableAdapter = new ShopMenager.ShopMenagerdbDataSetTableAdapters.Product_TableTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ppricesellDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ppricebuyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pstockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productTableBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.shopMenagerdb = new ShopMenager.ShopMenagerdb();
            this.productTableBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.product_TableTableAdapter1 = new ShopMenager.ShopMenagerdbTableAdapters.Product_TableTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productTableBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productTableBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 417);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(917, 33);
            this.panel2.TabIndex = 30;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(154, 314);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 26;
            this.button4.Text = "Menu";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(237, 276);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 25;
            this.button3.Text = "Usuń";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(154, 276);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "Edytuj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(73, 276);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Nazwa:";
            // 
            // ProdNameTb
            // 
            this.ProdNameTb.Location = new System.Drawing.Point(122, 143);
            this.ProdNameTb.Name = "ProdNameTb";
            this.ProdNameTb.Size = new System.Drawing.Size(136, 20);
            this.ProdNameTb.TabIndex = 19;
            this.ProdNameTb.TextChanged += new System.EventHandler(this.ProdNameTb_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Cena sprzedaży:";
            // 
            // ProdSellTb
            // 
            this.ProdSellTb.Location = new System.Drawing.Point(122, 176);
            this.ProdSellTb.Name = "ProdSellTb";
            this.ProdSellTb.Size = new System.Drawing.Size(136, 20);
            this.ProdSellTb.TabIndex = 20;
            this.ProdSellTb.TextChanged += new System.EventHandler(this.ProdSellTb_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Cena zakupu:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Kod produktu:";
            // 
            // ProdIdTb
            // 
            this.ProdIdTb.Location = new System.Drawing.Point(122, 109);
            this.ProdIdTb.Name = "ProdIdTb";
            this.ProdIdTb.Size = new System.Drawing.Size(136, 20);
            this.ProdIdTb.TabIndex = 18;
            this.ProdIdTb.TextChanged += new System.EventHandler(this.ProdIdTb_TextChanged);
            // 
            // shopMenagerdbDataSet
            // 
            this.shopMenagerdbDataSet.DataSetName = "ShopMenagerdbDataSet";
            this.shopMenagerdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(880, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 24);
            this.label6.TabIndex = 2;
            this.label6.Text = "X";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Book", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(360, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Zarządzaj produktami";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ProdBuyTb
            // 
            this.ProdBuyTb.Location = new System.Drawing.Point(122, 206);
            this.ProdBuyTb.Name = "ProdBuyTb";
            this.ProdBuyTb.Size = new System.Drawing.Size(136, 20);
            this.ProdBuyTb.TabIndex = 21;
            this.ProdBuyTb.TextChanged += new System.EventHandler(this.ProdBuyTb_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(917, 80);
            this.panel1.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Ilość:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // ProdStockTb
            // 
            this.ProdStockTb.Location = new System.Drawing.Point(122, 232);
            this.ProdStockTb.Name = "ProdStockTb";
            this.ProdStockTb.Size = new System.Drawing.Size(136, 20);
            this.ProdStockTb.TabIndex = 22;
            this.ProdStockTb.TextChanged += new System.EventHandler(this.ProdStockTb_TextChanged);
            // 
            // productTableBindingSource
            // 
            this.productTableBindingSource.DataMember = "Product_Table";
            this.productTableBindingSource.DataSource = this.shopMenagerdbDataSet;
            // 
            // product_TableTableAdapter
            // 
            this.product_TableTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pidDataGridViewTextBoxColumn,
            this.pnameDataGridViewTextBoxColumn,
            this.ppricesellDataGridViewTextBoxColumn,
            this.ppricebuyDataGridViewTextBoxColumn,
            this.pstockDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.productTableBindingSource2;
            this.dataGridView1.Location = new System.Drawing.Point(330, 98);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(542, 164);
            this.dataGridView1.TabIndex = 33;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // pidDataGridViewTextBoxColumn
            // 
            this.pidDataGridViewTextBoxColumn.DataPropertyName = "P_id";
            this.pidDataGridViewTextBoxColumn.HeaderText = "P_id";
            this.pidDataGridViewTextBoxColumn.Name = "pidDataGridViewTextBoxColumn";
            // 
            // pnameDataGridViewTextBoxColumn
            // 
            this.pnameDataGridViewTextBoxColumn.DataPropertyName = "P_name";
            this.pnameDataGridViewTextBoxColumn.HeaderText = "P_name";
            this.pnameDataGridViewTextBoxColumn.Name = "pnameDataGridViewTextBoxColumn";
            // 
            // ppricesellDataGridViewTextBoxColumn
            // 
            this.ppricesellDataGridViewTextBoxColumn.DataPropertyName = "P_pricesell";
            this.ppricesellDataGridViewTextBoxColumn.HeaderText = "P_pricesell";
            this.ppricesellDataGridViewTextBoxColumn.Name = "ppricesellDataGridViewTextBoxColumn";
            // 
            // ppricebuyDataGridViewTextBoxColumn
            // 
            this.ppricebuyDataGridViewTextBoxColumn.DataPropertyName = "P_pricebuy";
            this.ppricebuyDataGridViewTextBoxColumn.HeaderText = "P_pricebuy";
            this.ppricebuyDataGridViewTextBoxColumn.Name = "ppricebuyDataGridViewTextBoxColumn";
            // 
            // pstockDataGridViewTextBoxColumn
            // 
            this.pstockDataGridViewTextBoxColumn.DataPropertyName = "P_stock";
            this.pstockDataGridViewTextBoxColumn.HeaderText = "P_stock";
            this.pstockDataGridViewTextBoxColumn.Name = "pstockDataGridViewTextBoxColumn";
            // 
            // productTableBindingSource2
            // 
            this.productTableBindingSource2.DataMember = "Product_Table";
            this.productTableBindingSource2.DataSource = this.shopMenagerdb;
            // 
            // shopMenagerdb
            // 
            this.shopMenagerdb.DataSetName = "ShopMenagerdb";
            this.shopMenagerdb.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // productTableBindingSource1
            // 
            this.productTableBindingSource1.DataMember = "Product_Table";
            this.productTableBindingSource1.DataSource = this.shopMenagerdbDataSet;
            // 
            // product_TableTableAdapter1
            // 
            this.product_TableTableAdapter1.ClearBeforeFill = true;
            // 
            // MenageProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ProdStockTb);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ProdNameTb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ProdSellTb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ProdIdTb);
            this.Controls.Add(this.ProdBuyTb);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MenageProducts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.MenageProducts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdbDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productTableBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopMenagerdb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productTableBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ProdNameTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ProdSellTb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ProdIdTb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ProdBuyTb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ProdStockTb;
        private ShopMenagerdbDataSet shopMenagerdbDataSet;
        private System.Windows.Forms.BindingSource productTableBindingSource;
        private ShopMenagerdbDataSetTableAdapters.Product_TableTableAdapter product_TableTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ppricesellDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ppricebuyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pstockDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource productTableBindingSource1;
        private ShopMenagerdb shopMenagerdb;
        private System.Windows.Forms.BindingSource productTableBindingSource2;
        private ShopMenagerdbTableAdapters.Product_TableTableAdapter product_TableTableAdapter1;
    }
} 