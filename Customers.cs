﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ShopMenager
{
    public partial class Customers : Form
    {
        public Customers()
        {
            InitializeComponent();
        }
        SqlConnection Con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=C:\Users\barna\source\repos\ShopMenager\ShopMenagerdb.mdf;Integrated Security = True; Connect Timeout=30");
        void populate()
        {
            try
            {
                Con.Open();
                string Myquery = "select * from CustomersTable";
                SqlDataAdapter da = new SqlDataAdapter(Myquery, Con);
                SqlCommandBuilder builder = new SqlCommandBuilder(da);
                var ds = new DataSet();
                da.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                Con.Close();
            }
            catch
            {

            }
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                Con.Open();
                SqlCommand cmd = new SqlCommand("insert into CustomersTable values(" + CIdTb.Text + ",'" + CNameTb.Text + "','" + CPhoneTb.Text + "')", Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Klient dodany");
                Con.Close();
                populate();
            }
            catch
            {

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                dataGridView1.CurrentRow.Selected = true;
                CIdTb.Text = dataGridView1.Rows[e.RowIndex].Cells["C_id"].FormattedValue.ToString();
                CNameTb.Text = dataGridView1.Rows[e.RowIndex].Cells["C_name"].FormattedValue.ToString();
                CPhoneTb.Text = dataGridView1.Rows[e.RowIndex].Cells["C_nip"].FormattedValue.ToString();
                
            }
        }

        private void Customers_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'shopMenagerdbDataSet1.UserTable' table. You can move, or remove it, as needed.
            this.userTableTableAdapter.Fill(this.shopMenagerdbDataSet1.UserTable);
            populate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (CIdTb.Text == "")
            {
                MessageBox.Show("Wprowadź kod produktu");
            }
            else
            {
                Con.Open();
                string myquery = "delete from CustomersTable where C_id='" + CIdTb.Text + "';";
                SqlCommand cmd = new SqlCommand(myquery, Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Klient usunięty");
                Con.Close();
                populate();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Con.Open();
                SqlCommand cmd = new SqlCommand("update CustomersTable set C_id='" + CIdTb.Text + "',C_name='" + CNameTb.Text + "',C_nip='" + CPhoneTb.Text + "'where C_id='" + CIdTb.Text + "'", Con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Dane klienta zaktualizowane");
                Con.Close();
                populate();
            }
            catch
            {

            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            m.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            m.Show();
            this.Hide();
        }
    }
}
